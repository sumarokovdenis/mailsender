import datetime
from scanner.models import related_keywords
from scanner.models import keywords


def save_yandex_scan_related_keywords_to_db(data, related_kws_data):

    for keyword in related_kws_data:
        kw_obj = keywords.get_keywords_by_text(keyword)
        if not kw_obj:
            rel_kw_id = keywords.add_object((keyword, datetime.datetime.now()))
        else:
            rel_kw_id = kw_obj[0]

        related_kws = related_keywords.get_relation_by_keywords(data[4], rel_kw_id)

        if len(related_kws) == 0:
            related_keywords.add_list(
                (
                    (
                        data[4],
                        rel_kw_id,
                        datetime.datetime.now()
                    ),
                )
            )


