from scanner.models import yandex_scan, keywords, \
    related_keywords, yandex_scan_result, process_ysr, projects, regions

model_list = [yandex_scan, keywords, related_keywords, yandex_scan_result, process_ysr, projects, regions]


def drop_and_create_tables():

    for model in model_list:
        try:
            model.drop_table()

        except Exception as e:
            print(e)

        try:

            model.create_table()
        except Exception as e:
            print(e)