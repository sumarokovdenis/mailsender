# -*- coding: utf-8 -*-
import re
from datetime import datetime
from urllib.parse import urlsplit

import requests
from bs4 import BeautifulSoup

from sites.models import site_page
from sites.models import html_headers, html_anchors, page_emails
import logging

class AnchorType(object):
    internal = 'internal link'
    external = 'external link'
    hash_link = 'hash_link'
    mail_link = 'mail link'
    image = 'image'
    no_href = 'no href'
    unknown = 'unknown'


class SiteScrapper(object):
    def __init__(self, **kwargs):
        self.url = kwargs.get('url')
        self.site_id = kwargs.get('site_id')
        self.url_list = []
        self.max_url_list_size = 100000

    def get_anchor_type(self, bs_anchor):
        try:
            m_netloc = urlsplit(self.url).netloc
            m_scheme = urlsplit(self.url).scheme
            if bs_anchor.attrs.get('href') == '':
                return AnchorType.no_href

            parsed_url = urlsplit(bs_anchor.attrs.get('href'))

            r_image = re.compile(r"(?i)\.(jpg|png|gif|jpeg)$")

            if r_image.findall(parsed_url.path):
                return AnchorType.image

            if parsed_url.netloc == m_netloc and \
                            parsed_url.scheme == m_scheme:

                return AnchorType.internal

            elif parsed_url.netloc == '' and \
                            parsed_url.scheme == m_scheme:
                return AnchorType.internal

            elif parsed_url.netloc == m_netloc:
                return AnchorType.internal

            elif parsed_url.netloc == '' and \
                            parsed_url.path != '':
                return AnchorType.internal
            elif parsed_url.netloc == '' and \
                            parsed_url.path != '':
                return AnchorType.internal
            elif parsed_url.netloc == '' and \
                            parsed_url.path != '':
                if parsed_url.path[0] == '#':
                    return AnchorType.hash_link
            elif parsed_url.scheme == 'mailto':
                return AnchorType.mail_link

            else:
                return AnchorType.external

        except Exception as e:
            logging.error(e)
            return AnchorType.unknown

    def get_html_meta_tegs(self, html):
        title = ''
        description = ''
        try:
            bs = BeautifulSoup(html, 'html5lib')
            title_obj = bs.html.head.title
            if title_obj:
                title = title_obj.text

            description_obj = bs.find(attrs={'name': 'description'})
            if description_obj:
                description = description_obj.attrs.get('content')

        except Exception as e:
            logging.error(e)

        return title, description

    def get_html_anchors(self,  site_page_id, html):

        try:
            bs = BeautifulSoup(html, 'html5lib')

            anchors = bs.findAll('a')
            anchors_list = [[
                self.get_anchor_type(a),
                a.text,
                str(a),
                a.attrs.get('href'),
                site_page_id,
                datetime.now()] for a in anchors]

            return anchors_list

        except Exception as e:
            logging.error(e)

    def get_html_headers(self,  site_page_id, html):
        headers_list = []
        try:
            bs = BeautifulSoup(html, 'html5lib')
            for num in range(1, 6):
                teg_str = 'h{}'.format(str(num))
                headers = bs.findAll(teg_str)
                headers_list.extend(
                    [teg_str, h.text, str(h), site_page_id, datetime.now()] for h in headers
                )

        except Exception as e:
            logging.error(e)

        return headers_list

    def get_emails(self, site_page_id, html):
        emails_list = []
        try:
            email_pattern = re.compile('([\w\-\.]+@(?:\w[\w\-]+\.)+[\w\-]+)')

            emails_list.extend(email_pattern.findall(html))

        except Exception as e:
            logging.error(e)

        return [[e.lower(), site_page_id, datetime.now()] for e in emails_list]

    def scrap_page(self, url):

        result = {}

        self.url_list.append(url)

        site_page_id = site_page.add_object(
            [
                url,
                None,
                None,
                None,
                self.site_id,
                datetime.now()
            ]
        )

        try:
            result['url'] = url

            page_response = requests.get(url)

            page_response.encoding = 'utf-8'

            result['status_code'] = page_response.status_code

            site_page.update_status_code(site_page_id, page_response.status_code)


            if page_response.status_code == 200:
                page_html = page_response.text

                page_title, page_description = self.get_html_meta_tegs(page_html)

                result['page_title'] = page_title
                site_page.update_meta_title(site_page_id, page_title)

                result['page_description'] = page_description
                site_page.update_meta_description(site_page_id, page_description)

                try:
                    result['page_anchors'] = self.get_html_anchors(site_page_id, page_html)
                    html_anchors.add_list(result['page_anchors'])
                except Exception as e:
                    logging.error(e)
                try:
                    result['page_headers'] = self.get_html_headers(site_page_id, page_html)
                    html_headers.add_list(result['page_headers'])
                except Exception as e:
                    logging.error(e)

                result['emails'] = self.get_emails(site_page_id, page_html)

                page_emails.add_new_emails(result['emails'])

        except Exception as e:
            logging.error(url, e)
            result['status_code'] = 100
            site_page.update_status_code(site_page_id, result['status_code'])

        return result

    def scrap_anchor(self, anchor):
        try:
            if anchor[2] == AnchorType.internal \
                    and anchor[3] not in self.url_list:
                anchor_data = self.scrap_page(anchor[3])
                if anchor_data:
                    if anchor_data['status_code'] == 200:
                        for next_anchor in anchor_data['page_anchors']:
                            self.scrap_anchor(next_anchor)
                            if len(self.url_list) > self.max_url_list_size:
                                break

        except Exception as e:
            logging.error(e, anchor)


    def start(self):

        first_data = self.scrap_page(self.url)

        if first_data['status_code'] == 200:
            first_anchors = first_data['page_anchors']
            if first_anchors:
                for anchor in first_anchors:
                    self.scrap_anchor(anchor)

