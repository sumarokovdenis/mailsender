import datetime

import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE YandexScanResult  
                       (id BIGSERIAL PRIMARY KEY NOT NULL,
                        yandex_scan_id INTEGER,
                        FOREIGN KEY (yandex_scan_id)
                              REFERENCES YandexScan(id),
                        anchor_position INTEGER,
                        page_number INTEGER,
                        anchor_type VARCHAR (256),
                        anchor_href VARCHAR (1024),
                        created_at TIMESTAMP NOT NULL);''')


def get_new_objects():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                        SELECT * FROM YandexScanResult 
                        LEFT JOIN ProcessYandexScanResults ON YandexScanResult.id = ProcessYandexScanResults.yandex_scan_result_id
                        WHERE ProcessYandexScanResults.yandex_scan_result_id IS NULL;''')
            yandex_scan_results = cur.fetchall()
            return yandex_scan_results


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute("INSERT INTO YandexScanResult VALUES (DEFAULT, %s, %s, %s, %s, %s, %s);", data)


def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute("DROP TABLE YandexScanResult CASCADE;")


def save_results_to_db(data, anchors_list):
    nl = []
    for anchor in anchors_list:
        nl.append([data[0], *anchor, datetime.datetime.now()])

    add_list(nl)