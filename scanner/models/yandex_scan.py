import datetime

import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE YandexScan  
                           (id BIGSERIAL PRIMARY KEY NOT NULL,
                           keyword_id INTEGER,
                           FOREIGN KEY (keyword_id) REFERENCES Keywords(id),                
                           region_id INTEGER,
                           FOREIGN KEY (region_id) REFERENCES Regions(id),
                           created_at TIMESTAMP NOT NULL);''')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute("INSERT INTO YandexScan VALUES (DEFAULT, %s, %s);", data)


def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''DROP TABLE YandexScan CASCADE ;  
                           ''')


def get_data(yandex_scan_id):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''WITH msu AS (
                        SELECT * FROM YandexScan 
                        JOIN Keywords ON YandexScan.keyword_id = Keywords.id
                        WHERE YandexScan.id = %s)
                        SELECT * FROM msu JOIN Regions ON msu.region_id = Regions.id
                        ;
            ''', (yandex_scan_id, ))
            yandex_scan_data = cur.fetchone()
            return yandex_scan_data


def create_by_keyword_and_region(keyword_id, region_id):
    data = (keyword_id, region_id, datetime.datetime.now())
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute("INSERT INTO YandexScan VALUES (DEFAULT, %s, %s, %s) RETURNING id", data)
            ys_id = cur.fetchone()[0]
            return ys_id