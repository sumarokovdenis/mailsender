import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE RelatedKeywords  
                       (id BIGSERIAL PRIMARY KEY NOT NULL,
                        parent_id INTEGER,
                        FOREIGN KEY (parent_id) REFERENCES Keywords(id),
                        child_id INTEGER,
                        FOREIGN KEY (child_id) REFERENCES Keywords(id), 
                        created_at TIMESTAMP NOT NULL);''')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute("INSERT INTO RelatedKeywords  VALUES (DEFAULT, %s, %s, %s);", data)


def add_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                INSERT INTO RelatedKeywords  VALUES (DEFAULT, %s, %s, %s)
                  RETURNING id;
                  ''', data)
            return cur.fetchone()[0]


def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''DROP TABLE RelatedKeywords CASCADE;''')


def get_relation_by_keywords(keyword_id, related_keyword_id):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM RelatedKeywords 
                           WHERE RelatedKeywords.parent_id = %s 
                           AND RelatedKeywords.child_id = %s
                       ;''', (keyword_id, related_keyword_id))
            return cur.fetchall()