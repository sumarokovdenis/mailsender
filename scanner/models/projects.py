import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE Projects  
                           (id BIGSERIAL PRIMARY KEY NOT NULL,
                           name VARCHAR (1024) NOT NULL,
                           region_id INTEGER,
                           FOREIGN KEY (region_id) REFERENCES Regions(id),
                           keyword VARCHAR (1024) NOT NULL,
                           created_at TIMESTAMP NOT NULL);''')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute("INSERT INTO Projects VALUES (DEFAULT, %s, %s, %s, %s);", data)


def add_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                INSERT INTO Projects VALUES (DEFAULT, %s, %s, %s, %s)
                  RETURNING id;
                  ''', data)
            return cur.fetchone()[0]


def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute("DROP TABLE Projects CASCADE ;")


def get_all_objects():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT * FROM Projects;")
            return cur.fetchall()