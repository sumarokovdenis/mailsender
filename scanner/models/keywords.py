import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE Keywords  
                           (id BIGSERIAL PRIMARY KEY NOT NULL,
                           keyword VARCHAR (1024) NOT NULL,
                           created_at TIMESTAMP NOT NULL);''')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute("INSERT INTO Keywords VALUES (DEFAULT, %s, %s);", data)


def add_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute("INSERT INTO Keywords VALUES (DEFAULT, %s, %s) RETURNING id;", data)
            return cur.fetchone()[0]


def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''DROP TABLE Keywords CASCADE;  
                           ''')


def is_new_object(keyword):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT * FROM Keywords WHERE Keywords.keyword = %s", (keyword, ))
            if len(cur.fetchall()) > 0:
                return False
            return True


def get_new_keywords():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM Keywords 
                           LEFT JOIN YandexScan ON Keywords.id = YandexScan.keyword_id
                           WHERE YandexScan.keyword_id IS NULL;''')
            ms = cur.fetchall()
            return ms


def get_new_keywords_by_region(region_id):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM Keywords 
                           LEFT JOIN YandexScan ON Keywords.id = YandexScan.keyword_id
                           WHERE YandexScan.keyword_id IS NULL
                           OR YandexScan.region_id != %s
                           ;''', (region_id, ))
            ms = cur.fetchall()
            return ms


def get_keywords_by_text(keyword):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM Keywords 
                           WHERE Keywords.keyword = %s
                           ;''', (keyword, ))
            ms = cur.fetchone()
            return ms