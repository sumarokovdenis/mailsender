import time
from urllib.parse import urlsplit
from selenium.webdriver.common.keys import Keys
import requests

import logging
logger = logging.getLogger(__name__)


size_object = {
    'normal': (1920, 3648),
}


def set_region(browser, url, region):
    link_text = u'Настройка'

    browser.get(url)

    setting_link = browser.find_element_by_link_text(link_text)
    setting_link.click()

    time.sleep(5)

    change_city_link_text = u'Изменить город'

    change_city_link = browser.find_element_by_link_text(change_city_link_text)

    change_city_link.click()

    city_input_id = 'city__front-input'

    city_input = browser.find_element_by_id(city_input_id)

    city_input.clear()

    city_input.send_keys(region)

    time.sleep(3)

    city_input.send_keys(Keys.ENTER)

def input_key_word(browser, keyword):
    time.sleep(3)
    main_form_id = 'text'
    main_form = browser.find_element_by_id(main_form_id)

    main_form.send_keys(keyword)
    time.sleep(1)
    main_form.send_keys(Keys.ENTER)

def find_next_button(browser):
    time.sleep(3)
    element = browser.find_element_by_xpath(
        "//a[text()='дальше']")

    element.click()

def find_related_keywords(browser):
    elements = browser.find_elements_by_xpath(
        "//div[@class='related__item']/a")
    el_list = []
    for element in elements:
        el_list.append(element.text)
    return el_list

def normalize_url_use_requests(url):
    parsed_url = urlsplit(url)
    if parsed_url.scheme != "":
        response = requests.get(url)
    else:
        new_url = 'http://' + str(url)
        response = requests.get(new_url)
    response_url = response.url
    return response_url

def get_organic_link_from_li(li):
    anchors = li.find_elements_by_tag_name('a')
    for anchor in anchors:
        anchor_href = anchor.get_attribute('href')
        return anchor_href

def get_direct_link_from_li(li):

    path_div_class = 'path'

    path_div = li.find_element_by_class_name(path_div_class)
    anchors = path_div.find_elements_by_tag_name('a')

    for anchor in anchors:
        return normalize_url_use_requests(anchor.get_attribute('text'))

def is_li_direct(li):
    anchors = li.find_elements_by_tag_name('a')
    for anchor in anchors:
        anchor_href = anchor.get_attribute('href')
        is_direct = str(anchor_href).startswith('http://yabs.yandex.ru/count/') or str(anchor_href).startswith(
            'https://yabs.yandex.ru/count/')
        return is_direct

def is_li_ya_search(li):
    anchors = li.find_elements_by_tag_name('a')
    for anchor in anchors:
        anchor_href = anchor.get_attribute('href')
        is_ = str(anchor_href).startswith('https://www.yandex.ru') or str(anchor_href).startswith('http://www.yandex.ru')
        return is_

def is_li_ya_uslugi(li):
    anchors = li.find_elements_by_tag_name('a')
    for anchor in anchors:
        anchor_href = anchor.get_attribute('href')
        is_ = str(anchor_href).startswith('https://www.yandex.ru/uslugi/')
        return is_

def is_li_ya_video(li):
    anchors = li.find_elements_by_tag_name('a')
    for anchor in anchors:
        anchor_href = anchor.get_attribute('href')
        is_ = str(anchor_href).startswith('https://yandex.ru/video/')
        return is_

def is_li_ya_map(li):
    anchors = li.find_elements_by_tag_name('a')
    for anchor in anchors:
        anchor_href = anchor.get_attribute('href')
        is_ = str(anchor_href).startswith('https://yandex.ru/maps/')
        return is_



