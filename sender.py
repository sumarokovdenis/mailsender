import time
from urllib.parse import urlsplit, urlunsplit


from celery import Celery
from django.utils.datetime_safe import datetime
from mails.models import addressees
from scanner.models import keywords as skwrds
from scanner.models import process_ysr
from scanner.models import projects as prjts
from scanner.models import yandex_scan_result as ysr
from selenium import webdriver

from mails.emails import send_mail
from mails.models import mailsends
from scanner import tools, keywords
from scanner.models import yandex_scan as ys
from scanner.site_scrapper import SiteScrapper
from settings.conf import CH_MS_TIME, CN_NU_TIME, CH_NP_TIME, DEEP_LEVEL, SCRAP_ML, AUTO_SEND_FIRST_MAIL
from settings.conf import chrome_drive_path
from sites.models import site
import logging

app = Celery('tasks', broker='redis://localhost:6379/0')

app.conf.beat_schedule = {
    'check_new_users': {
        'task': 'sender.check_new_addressees',
        'schedule': CN_NU_TIME,
    },
    'check_new_mail_sends': {
        'task': 'sender.check_new_mail_sends',
        'schedule': CH_MS_TIME,
    },
    'check_new_keywords_in_projects': {
        'task': 'sender.check_new_keywords_and_start_yandex_scanner',
        'schedule': CH_NP_TIME,
    },
    'check_new_yandex_scan_results': {
        'task': 'sender.check_and_prepare_new_yandex_scan_results',
        'schedule': CH_NP_TIME,
    }

}

app.conf.timezone = 'UTC'


@app.task
def check_new_mail_sends():
    ms = mailsends.get_new_objects()
    for user_ms in ms:
        ch_dt = mailsends.check_journal(user_ms[1])
        if len(ch_dt) > 0:
            continue
        else:
            send_mail(user_ms[0])
            time.sleep(user_ms[4]/1000)  # in seconds


@app.task
def check_new_addressees():
    if AUTO_SEND_FIRST_MAIL:
        nu = addressees.get_new_objects()
        for u in nu:
            mailsends.create_first_mail_send(u[0])


@app.task
def yandex_scan(yandex_scan_id):

    data = ys.get_data(yandex_scan_id)
    url = u'https://yandex.ru'

    region = data[8]
    keyword = data[5]

    options = webdriver.ChromeOptions()
    options.add_argument("no-sandbox")
    options.add_argument('headless')

    browser = webdriver.Chrome(executable_path=chrome_drive_path,
                               chrome_options=options)
    tools.set_region(browser, url, region)

    tools.input_key_word(browser, keyword)

    result_class = 'serp-item'

    anchor_position = 0

    for page_number in range(0, DEEP_LEVEL):

        anchors_list = []

        results_li = browser.find_elements_by_class_name(result_class)

        for li in results_li:

            try:
                if tools.is_li_direct(li):
                    anchors_list.append([anchor_position,
                                         page_number,  'direct',
                                         tools.get_direct_link_from_li(li)])
                elif tools.is_li_ya_search(li):
                    anchors_list.append([anchor_position,
                                         page_number, 'ya_search',
                                         tools.get_organic_link_from_li(li)])
                elif tools.is_li_ya_video(li):
                    anchors_list.append([anchor_position,
                                         page_number, 'ya_video',
                                         tools.get_organic_link_from_li(li)])
                elif tools.is_li_ya_map(li):
                    anchors_list.append([anchor_position,
                                         page_number, 'ya_map',
                                         tools.get_organic_link_from_li(li)])
                elif tools.is_li_ya_uslugi(li):
                    anchors_list.append([anchor_position,
                                         page_number, 'ya_uslugi',
                                         tools.get_organic_link_from_li(li)])
                else:
                    anchors_list.append([anchor_position,
                                         page_number, 'organic',
                                         tools.get_organic_link_from_li(li)])

            except Exception as e:
                print(e, li, anchor_position)

            anchor_position += 1

        related_keywords = tools.find_related_keywords(browser)

        ysr.save_results_to_db(data, anchors_list)
        keywords.save_yandex_scan_related_keywords_to_db(data, related_keywords)

        tools.find_next_button(browser)

    browser.close()


@app.task
def check_new_keywords_and_start_yandex_scanner():

    projects = prjts.get_all_objects()
    for project in projects:

        if skwrds.is_new_object(project[3]):
            skwrds.add_list(
               ((project[3], datetime.now()),)
           )

        kw = skwrds.get_new_keywords_by_region(project[2])

        for k in kw:
            yandex_scan_id = ys.create_by_keyword_and_region(k[0], project[2])
            yandex_scan.apply_async(kwargs={'yandex_scan_id':yandex_scan_id})
            #yandex_scan(yandex_scan_id)


@app.task
def scrap_site_task(url, site_id):
    scrapper = SiteScrapper(url=url, site_id=site_id)
    scrapper.start()


@app.task
def check_and_prepare_new_yandex_scan_results():
    new_ys_results = ysr.get_new_objects()

    c = 0
    for ys_res in new_ys_results:

        if c == SCRAP_ML:
            break

        p_id = process_ysr.add_object(
            [ys_res[0], datetime.now()])

        if site.is_new_site(p_id):
            c += 1
            us = urlsplit(ys_res[5])
            url = urlunsplit([
                us[0], us[1], '', '', '']
            )

            site_id = site.add_object(
                [url, p_id, datetime.now()]
            )

            scrap_site_task.apply_async(kwargs={'url': url, 'site_id': site_id})
            #scrap_site_task(url, site_id)
