from scanner.models import projects, regions
from datetime import datetime

if __name__ == '__main__':
    region_data = ['Cанкт-Петербург' , datetime.now()]
    region_id = regions.add_object(region_data)
    project_data = [
        'White Code', # имя проекта
        region_id,    # регион поиска адресатов
        'разработка и продвижение собственных продуктор и сервисов', # рутовое ключевое слово проекта
        datetime.now() # дата создания
    ]
    projects.add_object(project_data)