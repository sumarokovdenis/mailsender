from sites.db import drop_and_create_tables as sdb
from mails.db import drop_and_create_tables as mdb
from scanner.db import drop_and_create_tables as scdb


db_list = [mdb, sdb, scdb]


if __name__ == '__main__':
    for db in db_list:
        db()