import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE Addressees  
                           (id BIGSERIAL PRIMARY KEY NOT NULL,
                           name VARCHAR (256) NOT NULL,
                           email VARCHAR (256) NOT NULL,
                           created_at TIMESTAMP NOT NULL);''')


def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('DROP TABLE Addressees CASCADE ;')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute("INSERT INTO Addressees VALUES (DEFAULT, %s, %s, %s)", data)


def get_new_objects():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM Addressees LEFT JOIN MailSends 
                            ON Addressees.id = MailSends.addressees_id 
                            WHERE MailSends.addressees_id IS NULL;
                           ''')
            nu = cur.fetchall()
            return nu


def get_all_objects():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('SELECT * FROM Addressees;')
            users = cur.fetchall()
            return users

def is_new_object_by_email(email):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM Addressees WHERE Addressees.email = %s;
                           ''', (email,))
            nu = cur.fetchall()
            if len(nu) > 0:
                return False
            return True

def add_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                INSERT INTO Addressees VALUES (DEFAULT, %s, %s, %s)
                  RETURNING id;
                  ''', data)
            return cur.fetchone()[0]


def get_object_id_by_email(email):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                SELECT * FROM Addressees WHERE email = %s;
                  ''', (email, ))
            return cur.fetchone()[0]

def get_object_by_id(addressees_id):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM Addressees WHERE Addressees.id = %s;
                           ''', (addressees_id,))
            return cur.fetchone()