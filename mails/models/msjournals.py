import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE MSJournals  
                           (id BIGSERIAL PRIMARY KEY NOT NULL,
                           mail_send_id INTEGER,
                           FOREIGN KEY (mail_send_id) REFERENCES MailSends(id),
                           created_at TIMESTAMP NOT NULL);''')


def add_object(ms_id):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''INSERT INTO MSJournals (mail_send_id, created_at)
                           VALUES (%s, now()) RETURNING id''', (ms_id, ))
            return cur.fetchone()[0]


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute("INSERT INTO MSJournals VALUES (%s, %s, %s);", data)


def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''DROP TABLE MSJournals CASCADE;  
                           ''')