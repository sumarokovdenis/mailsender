import datetime

import psycopg2
from mails.models import addressees
from settings.conf import DB, SEND_DELAY, PERIODIC_DELAY


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE MailSends  
                           (id BIGSERIAL PRIMARY KEY NOT NULL,
                           email VARCHAR (256) NOT NULL,
                           name VARCHAR (256) NOT NULL,
                           subject TEXT,
                           content TEXT NOT NULL,
                           addressees_id INTEGER,
                           FOREIGN KEY (addressees_id) REFERENCES Addressees(Id),                         
                           send_delay INTEGER,
                           periodic_delay VARCHAR (256),
                           send_priority INTEGER,
                           created_at TIMESTAMP NOT NULL);''')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute("INSERT INTO MailSends VALUES (DEFAULT, %s, %s, %s, %s, %s, %s, %s, %s, %s)", data)


def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''DROP TABLE MailSends CASCADE ;  
                           ''')


def get_object_by_id(ms_id):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute("""SELECT * FROM MailSends
                           WHERE id = %s
                      ; """, (ms_id, ))
            return cur.fetchone()

def get_new_objects():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''WITH msu AS (
                                        SELECT MailSends.id, Addressees.id as addressees_id, MailSends.created_at, MailSends.send_priority, MailSends.send_delay 
                                        FROM MailSends JOIN Addressees ON MailSends.addressees_id = Addressees.id ORDER BY MailSends.send_priority, MailSends.created_at
                                        )
                           SELECT * FROM msu LEFT JOIN MSJournals ON msu.id = MSJournals.mail_send_id 
                           WHERE MSJournals.mail_send_id IS NULL;
                           ''')
            mailsends = cur.fetchall()
            return mailsends


def get_mailsends_addressees(addressees_id):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM MailSends, Addressees 
                           WHERE MailSends.addressees_id = %s 
                           AND MailSends.is_send = FALSE AND Addressees.id = %s;''',
                        (addressees_id, addressees_id))
            mailsends = cur.fetchall()
            return mailsends


def check_journal(addressees_id):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM MailSends JOIN MSJournals ON MailSends.id = MSJournals.mail_send_id 
                           WHERE MailSends.addressees_id = %s
                            AND MailSends.created_at > now() - MailSends.periodic_delay::INTERVAL
                           ;''', (addressees_id, )
                           )
            ms = cur.fetchall()
            return ms


def create_first_mail_send(addressees_id):

    addressees_data = addressees.get_object_by_id(addressees_id)

    data = (
        addressees_data[2],
        addressees_data[1],
        'Мы рады Вас приветствовать',
        'Уважаемый {}, заходите на наш сервис!'.format(addressees_data[1]),
        addressees_id,
        SEND_DELAY,
        PERIODIC_DELAY,
        1,
        datetime.datetime.now()
    )
    add_list([data])