import smtplib as smtp

from mails.models import msjournals, mailsends
from settings.conf import email, password


def send(data):

    dest_email = data[1]
    name = data[2]
    subject = data[3]
    email_text = data[4]

    message = 'From: {}\nTo: {}\nSubject: {}\n\n{}'.format(email,
                                                           dest_email,
                                                           subject,
                                                           email_text)

    server = smtp.SMTP_SSL('smtp.yandex.com')
    server.set_debuglevel(1)
    server.ehlo(email)
    server.login(email, password)
    server.auth_plain()
    server.sendmail(email, dest_email, message)
    server.quit()


def send_mail(ms_id):
    msjournals.add_object(ms_id)

    ms_data = mailsends.get_object_by_id(ms_id)
    send(ms_data)