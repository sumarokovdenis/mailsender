from mails.models import msjournals, mailsends, addressees

models_list = [msjournals, mailsends, addressees]


def drop_and_create_tables():
    for model in models_list:
        try:
            model.drop_table()
        except Exception as e:
            print(e)

    models_list.reverse()

    for model in models_list:
        try:
            model.create_table()
        except Exception as e:
            print(e)