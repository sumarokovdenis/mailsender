import os

DB_NAME = 'mailsender'
DB_USER = 'mailsender_user'
DB_USER_PASSWORD = 'mailspassword'
DB_HOST = '127.0.0.1'
DB_PORT = '5432'

DB = {
      'models': DB_NAME,
      'user': DB_USER,
      'password': DB_USER_PASSWORD,
      'host': DB_HOST,
      'port': DB_PORT
}

SEND_DELAY = 5 #mkseconds
PERIODIC_DELAY = '10 minutes'

CH_MS_TIME = 1 * 60 #seconds
CN_NU_TIME = 1 * 60 #seconds

CH_NP_TIME = 1 * 60 #seconds

DEEP_LEVEL=10

SCRAP_ML = 10

AUTO_SEND_FIRST_MAIL = False

email = 'admin@mail.ru'
password = 'pass01'

keywords_file = 'keywords.txt'
chrome_drive_path = '/'.join([os.path.abspath(os.curdir), 'chromedriver'])

