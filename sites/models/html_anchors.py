import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE HtmlAnchors  
                          (id BIGSERIAL PRIMARY KEY NOT NULL,
                           anchor_type VARCHAR (2048),
                           text VARCHAR (2048),
                           html VARCHAR (4096),
                           href VARCHAR (4096),
                           site_page_id INTEGER,
                           FOREIGN KEY (site_page_id)
                              REFERENCES SitePage(id),
                           created_at TIMESTAMP NOT NULL);''')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:

            for data in data_list:
                cur.execute('''
                    INSERT INTO HtmlAnchors
                      VALUES (DEFAULT , %s, %s, %s, %s, %s, %s)
                ;''', data)


def add_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                INSERT INTO HtmlAnchors
                      VALUES (DEFAULT , %s, %s, %s, %s, %s, %s)
                  RETURNING id;
                  ''', data)
            return cur.fetchone()[0]

def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
            DROP TABLE HtmlAnchors
            ;''')