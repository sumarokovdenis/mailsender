import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE SitePage  
                          (id BIGSERIAL PRIMARY KEY NOT NULL,
                           url VARCHAR (2048),
                           meta_title VARCHAR (2048),
                           meta_description VARCHAR (2048),
                           status_code INTEGER,
                           site_id INTEGER,
                           FOREIGN KEY (site_id)
                              REFERENCES Site(id),
                           created_at TIMESTAMP NOT NULL);''')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute('''
                    INSERT INTO SitePage
                      VALUES (DEFAULT , %s, %s, %s, %s, %s, %s)
                ;''', data)


def add_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                INSERT INTO SitePage
                      VALUES (DEFAULT , %s, %s, %s, %s, %s, %s)
                  RETURNING id;
                  ''', data)
            return cur.fetchone()[0]


def update_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                INSERT INTO SitePage
                      VALUES (%s , %s, %s, %s, %s, %s, %s)
                  RETURNING id;
                  ''', data)
            return cur.fetchone()[0]

def update_status_code(site_page_id, status_code):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                UPDATE SitePage SET status_code = %s WHERE id = %s
                  RETURNING id;
                  ''', (status_code, site_page_id))
            conn.commit()
            return cur.fetchone()[0]

def update_meta_title(site_page_id, val):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                UPDATE SitePage SET meta_title = %s WHERE id = %s
                  RETURNING id;
                  ''', (val, site_page_id))
            conn.commit()
            return cur.fetchone()[0]

def update_meta_description(site_page_id, val):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                UPDATE SitePage SET meta_description = %s WHERE id = %s
                  RETURNING id;
                  ''', (val, site_page_id))
            conn.commit()
            return cur.fetchone()[0]

def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
            DROP TABLE SitePage
            ;''')