import psycopg2

from settings.conf import DB
from mails.models import addressees


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE PageEmails  
                          (id BIGSERIAL PRIMARY KEY NOT NULL,
                           addressees_id INTEGER,
                           FOREIGN KEY (addressees_id)
                              REFERENCES Addressees(id),
                           site_page_id INTEGER,
                           FOREIGN KEY (site_page_id)
                              REFERENCES SitePage(id),
                           created_at TIMESTAMP NOT NULL);''')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute('''
                    INSERT INTO PageEmails
                      VALUES (DEFAULT , %s, %s, %s, %s)
                ;''', data)


def add_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            try:
                cur.execute('''
                    INSERT INTO PageEmails
                          VALUES (DEFAULT , %s, %s, %s)
                      RETURNING id;
                      ''', data)
                return cur.fetchone()[0]
            except Exception as e:
                print(e)

def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
            DROP TABLE PageEmails
            ;''')



def add_new_emails(data_list):
    for email_data in data_list:

        email_name = email_data[0].split('@')[0]
        email = email_data[0]

        if addressees.is_new_object_by_email(email):
            email_data[0] = addressees.add_object((email_name, email_data[0], email_data[2]))
        else:
            addressees_id = addressees.get_object_id_by_email(email_data[0])
            email_data[0] = addressees_id

        add_object(email_data)