import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE ScrapSite  
                          (id BIGSERIAL PRIMARY KEY NOT NULL,
                           site_id INTEGER,
                           FOREIGN KEY (site_id)
                              REFERENCES Site(id),
                           created_at TIMESTAMP NOT NULL);''')


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute('''
                    INSERT INTO ScrapSite  VALUES (DEFAULT , %s, %s)
                ;''', data)


def add_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                INSERT INTO ScrapSite  VALUES (DEFAULT , %s, %s)
                  RETURNING id;
                  ''', data)
            return cur.fetchone()[0]

def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
            DROP TABLE ScrapSite
            ;''')