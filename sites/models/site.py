import psycopg2

from settings.conf import DB


def create_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''CREATE TABLE Site  
                          (id BIGSERIAL PRIMARY KEY NOT NULL,
                           url VARCHAR (1024),
                           process_yandex_scan_result_id INTEGER,
                           FOREIGN KEY (process_yandex_scan_result_id)
                              REFERENCES ProcessYandexScanResults(id),
                           created_at TIMESTAMP NOT NULL);''')


def is_new_site(process_yandex_scan_result_id):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''SELECT * FROM Site  
                           WHERE Site.process_yandex_scan_result_id = %s;''',
                        (process_yandex_scan_result_id, ))
            if len(cur.fetchall()) > 0:
                return False
            return True


def add_list(data_list):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            for data in data_list:
                cur.execute('''
                    INSERT INTO Site
                      VALUES (DEFAULT , %s, %s, %s)
                ;''', data)


def add_object(data):
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
                INSERT INTO Site
                      VALUES (DEFAULT , %s, %s, %s)
                  RETURNING id;
                  ''', data)
            return cur.fetchone()[0]


def drop_table():
    with psycopg2.connect(**DB) as conn:
        with conn.cursor() as cur:
            cur.execute('''
            DROP TABLE Site
            ;''')