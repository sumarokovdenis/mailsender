from sites.models import site, site_page, scrap_site, html_headers, html_anchors, page_emails
from mails.models import addressees

models_list = [page_emails, html_anchors, html_headers, scrap_site, site_page, site, addressees]


def drop_and_create_tables():
    for model in models_list:
        try:
            model.drop_table()
        except Exception as e:
            print(e)

    models_list.reverse()

    for model in models_list:
        try:
            model.create_table()
        except Exception as e:
            print(e)

